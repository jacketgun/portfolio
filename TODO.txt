- Passwords should be stored in the database as hashes, not naked text. (django does this by default)

- searching the database for usernames should be made efficient, searching the whole thing is not scalable.
(not that this app is going to get big but it's still the best practice). (ditto)

- Passwords should need to be validated, they should need:  
- a Big letter
- a minimum amount of characters
- no special characters
- atleast one number
- and a maximum amount of characters allowed.


- tell the user if their registration is invalid without reloading the page. and email recovery. X

- error pages X

- 2 step authentication (optional) X

- learn selenium --:

- PAGINATION FOR POSTS


- footer should be below everything, nothing should clip into it.

- make the title tag change based on which page the user is viewing.


color list:
#a3a3c2 light gray - background.
#0a0a10 dark gray, almost black - box border.
#e0e0eb basically white - box background.
#ff4d4d light red - delete buttons.
#00cc00 dark green - form borders.

from django.test import TestCase, LiveServerTestCase, Client
from django.contrib.auth.models import User
from main.models import UserDescription, Post
from django.http import HttpResponse
from django.contrib.auth import authenticate
#from test.support import EnvironmentVarGuard
from django.shortcuts import get_object_or_404
from django.core.mail import send_mail


def create_user(username, email, password): # this is just here for convenience
	return User.objects.create_user(username=username,email=email, password=password)
	
def create_description(username):			# this is here because i had to include it because of spaghetti, if i had created a custom user model at the start of the project i could skip this.
	return UserDescription.objects.create(owner=User.objects.get(username=username),description="test")
	
class DatabaseTests(TestCase):
# defs MUST start with test_ otherwise they wont even run!!!!!
	def test_username_is_valid(self):                # this test checks if the username is in the database or not. is true if it exists.
		
		create_user(username='kekek',email='memes@memes.kek',password='botbot') # defines a username and password
		self.assertTrue(User.objects.filter(username='kekek'))
		
	def test_username_is_not_valid(self):            # this test checks if the username given is not in the database, because things that dont exist should not get a pass.
		
		create_user(username='paokpak', email='aosjdosj@asojdojd.asjdaj', password='kkd')
		self.assertFalse(User.objects.filter(username='memes'))
		
	def test_kek(self):								 # this test checks if descriptions are properly created.
		create_user(username="memes", email="asokaokd@asjd.asdo", password="memes") # descriptions must be linked to a user.
		create_description("memes")
		self.assertEqual(str(UserDescription.objects.get(owner__username="memes")), 'test')

		
class ServerTests(LiveServerTestCase):
	
	def test_login(self):				# tests if the application correctly logs the user in.
		create_user(username="admin", email="kek@kek.kek", password="admin")
		response = self.client.post('/main/auth/SignIn', {'username': 'admin', 'password': 'admin'})
		self.assertEqual(response.status_code, 302)        # status code 302 means that this post got redirected, which means that i did in fact log in
		
	def test_login_false_positive(self):		# tests if the application properly denies users that do not exist.
		response = self.client.post('/main/auth/SignIn', {'username': 'admin', 'password': 'admin'})
		self.assertNotEqual(response.status_code, 302)    # since i do not get redirected when login fails, this should not be Equal.
		
	def test_registration(self):				# tests if the user can use the site to register a user.
		register = self.client.post('/main/auth/register', {'username': 'userboy', 'email':'hue@hue.hue', 'password': 'secret', 'password2': 'secret'})
		self.assertTrue(User.objects.get(username="userboy"))
		
	def test_overlapping_registration(self):	# tests that the application does not allow multiple users with the same email to register.
												# There is probably little harm in allowing a person to have multiple accounts on one email, i just made it this way in order to learn to do it.
		register = self.client.post('/main/auth/register', {'username': 'userboy', 'email':'hue@hue.hue', 'password': 'secret', 'password2': 'secret'})
		register2 = self.client.post('/main/auth/register', {'username': 'u2erboy', 'email':'hue@hue.hue', 'password': 'secret', 'password2': 'secret'})
		register
		register2
		self.assertFalse(User.objects.filter(username='u2erboy').exists())
		
	def test_edit_user_description(self):		# tests to see if a user that is registered and logged in can edit their profile.
		register = self.client.post('/main/auth/register', {'username': 'userboy', 'email':'hue@hue.hue', 'password': 'secret', 'password2': 'secret'})
		login = self.client.post('/main/auth/SignIn', {'username': 'userboy', 'password': 'secret'})
		edit = self.client.post('/main/user/profile_edit', {'edit': 'custom'})
		register
		login
		edit
		self.assertEqual(str(UserDescription.objects.get(owner__username='userboy')), 'custom')
		
	def test_post_creation(self):				# tests to see if a user that is registered and logged in can post comments on the dummy page.
		register = self.client.post('/main/auth/register', {'username': 'userboy', 'email':'hue@hue.hue', 'password': 'secret', 'password2': 'secret'})
		login = self.client.post('/main/auth/SignIn', {'username': 'userboy', 'password': 'secret'})
		post = self.client.post('/main/dummy', {'post': 'memes'})
		register
		login
		post
		self.assertEqual(str(Post.objects.get(author__username='userboy', pk=1)), 'memes')
		
	def test_post_deletion(self):				# tests to see if a user that is registered and logged in can delete comments that they have posted.
		register = self.client.post('/main/auth/register', {'username': 'userboy', 'email':'hue@hue.hue', 'password': 'secret', 'password2': 'secret'})
		login = self.client.post('/main/auth/SignIn', {'username': 'userboy', 'password': 'secret'})
		post = self.client.post('/main/dummy', {'post': 'memes'})
		delete = self.client.post('/main/dummy', {'delete': 'Delete', 'oiy': 'memes' , 'oin': '2'})
		register
		login
		post
		delete
		try:
			dtest = Post.objects.get(pk=2)
			self.assertTrue(False)
		except Post.DoesNotExist:
			pass
		self.assertTrue(True)
		
		
	def test_email_recovery(self):				# tests that the email recovery system successfully sends an email and that the recovery page properly changes a users password.
		register = self.client.post('/main/auth/register', {'username': 'userboy', 'email':'TrymPortfolio@outlook.com', 'password': 'secret', 'password2': 'secret'})
		recover = self.client.post('/main/auth/recovery', {'email': 'TrymPortfolio@outlook.com'})
		recovered = self.client.post('/main/auth/recovered/' + self.client.session['value'], {'password1': 'memes', 'password2': 'memes'})
		user = (User.objects.get(email='TrymPortfolio@outlook.com'))
		register
		recover
		recovered
		self.assertEqual(user.password, 'memes' )
		
	def test_email_recovery_nonexistant_user(self):			# tests that nonexistant users cant recieve an email nor change the password of a user.
		register = self.client.post('/main/auth/register', {'username': 'userboy', 'email':'hue@hue.hue', 'password': 'secret', 'password2': 'secret'})
		recover = self.client.post('/main/auth/recovery', {'email': 'h2e@h2e.h2e'})
		user = (User.objects.get(email='hue@hue.hue'))
		register
		recover
		try:
			recovered = self.client.post('/main/auth/recovered/' + self.client.session['value'], {'email': 'h2e@h2e.h2e', 'password1': 'memes', 'password2': 'memes'})
			recovered
		except User.DoesNotExist:
			pass
		self.assertNotEqual(user.password, 'memes' )
		
from django import forms

class UsernameForm(forms.Form):   # gets rendered as 2 fields that contain a username and password.
	username = forms.CharField(label='Username goes here', max_length=100)
	password = forms.CharField(label='Password goes here', max_length=100)
	

	
class RegisterForm(forms.Form):
	username = forms.CharField(label='Username goes here', max_length=32)
	password = forms.CharField(label='Password goes here', max_length=32)
	password2 = forms.CharField(label='Re-enter your password', max_length=32)
	email = forms.EmailField(label='Enter your email here')
	
class SearchForm(forms.Form):
	search = forms.CharField(label='search', max_length=32)
	
	
class EditForm(forms.Form):
	edit = forms.CharField(max_length=140)
	
class PostForm(forms.Form):
	post = forms.CharField(max_length=300)
	
class RecoveryForm(forms.Form):
	email = forms.EmailField(label='Email:')
	
class RecoveryForm2(forms.Form):
	password1 = forms.CharField(label='Enter your new password.', max_length=99)
	password2 = forms.CharField(label='Again!', max_length=99)

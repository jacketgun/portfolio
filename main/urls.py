from django.urls import path
from . import views

# handler404 = 'views.notfound'
# handler500 = 'views.oopsiewoopsie'

urlpatterns = [
	path('', views.index, name="index"),                                # localhost:8000/main/
	path('base', views.base, name="base"),								# You are not supposed to be here.
	path('auth/SignIn', views.SignIn, name="SignIn"),                   # localhost:8000/main/auth/SignIn
	path('auth/SignIn2', views.SignIn2, name="SignIn2"),				# this is the google sign in page.
	path('auth/SignOut', views.SignOut, name="SignOut"),                # localhost:8000/main/auth/SignOut
	path('auth/register', views.register, name='register'),             # localhost:8000/main/auth/register
	path('user/profile_edit', views.profile_edit, name='profile_edit'), # localhost:8000/main/user/profile_edit
	path('user/<str:username>', views.profile, name='profile'),         # localhost:8000/main/user/(username) Note that if you change the length of this url you will need to edit the profile view accordingly.
	path('auth/recovery', views.recovery, name='recovery'),				# localhost:8000/main/auth/recovery
	path('auth/recovered/<str:value>', views.recovered, name='recovered'),
	path('dummy', views.dummy, name='dummy'),							# localhost:8000/main/dummy   This is a test page for comments.
	path('test', views.test, name='test'),
]
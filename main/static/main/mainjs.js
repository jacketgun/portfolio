function loadDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {            // defines something to happen when the readystate property changes
    if (this.readyState == 4 && this.status == 200) {/* this function is run every time the readystate changes.
														if both the readystate is at 4 (finished and ready) and the document that calls the  
														function is responding properly, it sets the content of 'demo'
														to the responsetext of this function which is loadDoc */
      document.getElementById("demo").innerHTML =
      this.responseText;
    }
  };
  xhttp.open("GET", "/static/main/ajax_info.txt", true); // the information is processed regardless of what the readystate is.
  xhttp.send();											 // but it is not used in any way unless the innerHTML of "demo" is set to be the response.
}


function recovery() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200){
			document.getElementById("emailblah").innerHTML = this.responseText;
		}
	};
	xhttp.open("GET", "/static/main/recovery.txt", true);
	xhttp.send();
	
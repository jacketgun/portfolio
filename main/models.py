from django.db import models
from django.contrib.auth.models import User


class UserDescription(models.Model): 		# This is the model for the descriptions of users.
	description = models.TextField()
	owner = models.ForeignKey(User, on_delete=models.CASCADE)
	
	
	def __str__(self):
		return self.description
		
class Post(models.Model):
	content = models.TextField()
	author = models.ForeignKey(User, on_delete=models.CASCADE)
	
	def __str__(self):
		return self.content
	

	
	
"""
class User(models.Model):   # models.Model is the database that the application uses, and is defined in the settings.py file.
	# An ID field is put in here by default, so i do not need to add one.
	username = models.CharField(max_length=32)       # Username of user.
	email = models.CharField(max_length=64, default="kek@kek")         # i do not want multiple users to create accounts on one email, as that makes it easier for bots register accounts. and that is BAD! (atleast i assume that is why this matters)
	password = models.CharField(max_length=20, default="no")					   # no real restrictions needed for a password, other than not leaving it blank
	
	def __str__(self): # returns question_text as a string instead of the data.
		return self.username
		
	def username_exists(self):   # returns a boolean based on wether or not the publish date is less than a day old.
		return self.username
		
	def email_exists(self):
		return self.email
		
	def password_exists(self):
		return self.password
"""
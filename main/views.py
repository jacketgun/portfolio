from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import UsernameForm, RegisterForm, SearchForm, EditForm, PostForm, RecoveryForm, RecoveryForm2
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, UserManager
from django.core import validators
from .models import UserDescription, Post
from django.urls import reverse 
from django.core.mail import send_mail
from django.utils.crypto import get_random_string
from django.template.loader import render_to_string
from django.core.paginator import Paginator
from django.shortcuts import render

keylength = 32


def test(request):
	return render(request, "main/test.html")

def index(request):  # request turns to localhost:8000.
	searchform = SearchForm()
	if request.user.is_authenticated:
		return render(request, 'main/index.html', {'searchform': searchform, 'login': True})
	return render(request, 'main/index.html', {'searchform': searchform})

def base(request):
	return render(request, 'main/base.html')

def SignIn(request):
	if request.user.is_authenticated:				   # lets me test for sure if user is logged in.
		return redirect('/main/')
	form = UsernameForm()
	if request.method == 'POST':                       # If the page is accessed by means of POST, try to log in the user.
		form = UsernameForm(request.POST)
		if form.is_valid():
			usern = form.cleaned_data.get('username')			   # The only way to access this page is via trying to log in anyway, so it should be fine.
			passw = form.cleaned_data.get('password')
			user = authenticate(request, username=usern, password=passw) # checks the database for the current information.
			if user is not None:						   # If the user does in fact exist log them in.
				login(request, user)
				return redirect(reverse('index')) # If the user is valid, log them in and redirect them to the index page.
			return render(request, 'main/auth/SignIn.html', {'userform': form, 'invalid': True})	
	return render(request, 'main/auth/SignIn.html', {'userform': form})  # if the user is not accessing this page via post then show them the SignIn page.
	
	
def SignIn2(request):
	return render(request, 'main/auth/SignIn2.html')

def SignOut(request):
	logout(request)
	return redirect('/main/')
	
	
def register(request):
	if request.user.is_authenticated:
		logout(request)
	if request.method == "POST":
		form = RegisterForm(request.POST) # request.post is all of the information POSTed from the page, which is the username email passwords etc. in this case.
										  # i also specify that only information from the RegisterForm be read here, because not doing to could potentially cause mishaps later on in development.
		if form.is_valid():               # cleans the data from the form (eg removes the text and saves it as variables)
			usernam = form.cleaned_data.get('username')
			mailE = form.cleaned_data.get('email')
			pass1 = form.cleaned_data.get('password')
			pass2 = form.cleaned_data.get('password2')
			checkname = User.objects.filter(username=usernam)
			checkemail = User.objects.filter(email=mailE)
			if pass1 == pass2:
				if usernam == "profile_edit":
					return render(request, 'main/auth/register.html', {'registerform': form, 'invalid': True})
				if checkname.exists():
					return render(request, 'main/auth/register.html', {'registerform': form, 'taken': True})
				if checkemail.exists():
					return render(request, 'main/auth/register.html', {'registerform': form, 'etaken': True})
				sav = User.objects.create_user(username=usernam, email=mailE, password=pass1)
				sav.save()
				desc = UserDescription.objects.create(owner=User.objects.get(username=usernam), description='')
				desc.save()
				return render(request, 'main/auth/register.html', {'registerform': form, 'registered': True})
	form = RegisterForm()
	return render(request, 'main/auth/register.html', {'registerform': form})
	
def profile(request, username):
	if request.user.is_authenticated:
		boi = request.user.username
		memo = request.path
		mem2 = memo[11:]
		userdescription = UserDescription.objects.get(owner__username=mem2).description
		#print(request.path)
		memo = request.path
		#print(memo[11:])
		return render(request, 'main/user/profile.html', {'userdescription': userdescription , 'username' : username}) # request.user.username should be the profiles owner, not the guy who is logged in, but this is for testing.
	return redirect(reverse('index')) # redirect originates from where you are, not the base directory! a reverse is needed!
	
	
def profile_edit(request):
	if request.user.is_authenticated:
		if request.method == "POST":
			form = EditForm(request.POST)
			
			if form.is_valid():
				info = form.cleaned_data.get('edit')
				sav = UserDescription.objects.get(owner__username=request.user.username)
				sav.description = info 
				sav.save()
				return redirect(reverse('profile', args=[request.user.username]))
		form = EditForm()
		return render(request, 'main/user/profile_edit.html', {'editform' : form, 'currentdescription': UserDescription.objects.get(owner__username=request.user.username).description})
	return redirect(request, 'main/')
	
	
def dummy(request):
	if request.user.is_authenticated:
		request.session.cycle_key()
		if request.method == "POST":
			form = PostForm(request.POST)
			
			if 'delete' in request.POST:
				deli = request.POST.get('oiy')
				kekp = request.POST.get('oin')
				Post.objects.filter(author__username=request.user.username,
									content=deli, pk=kekp).delete()
			if form.is_valid():
				info = form.cleaned_data.get('post')
				kek = Post.objects.create(author=User.objects.get
										(username=request.user.username),
										content=info)
				kek.save()
		
		commentlist = Post.objects.all().order_by("-pk")
		paginator = Paginator(commentlist,10) # display 10 comments per page.
		form = PostForm()
		page = request.GET.get('page')
		comments = paginator.get_page(page)
		return render(request, 'main/dummy.html', {'postform' : form, 'posts' : comments})
	return redirect(reverse('index'))
	
	
def recovery(request):
	if request.user.is_authenticated:
		return redirect(reverse('index'))
	request.session['value'] = get_random_string(length=keylength)

	form = RecoveryForm()
	if request.method == "POST":
		form = RecoveryForm(request.POST)
		if form.is_valid():
			email = form.cleaned_data.get('email')
			try:
				usr = str(User.objects.get(email=email))
				request.session['user'] = usr
			except User.DoesNotExist:
				return render(request, 'main/auth/recovery.html' , {'recoveryform' : form, 'nosend' : True})
			request.session['email'] = ''
			if User.objects.filter(email=email).exists():
				key = request.session.get('value')
				template = render_to_string('main/email/Email_template.html', {'link': key})
				plain = render_to_string('main/email/Email_template.txt', {'link': key})
				send_mail('Password Recovery', plain, 'TrymPortfolio@outlook.com', [email], html_message = template)
				return render(request, 'main/auth/recovery.html', {'recoveryform' : form, 'sent': True})
	return render(request, 'main/auth/recovery.html', {'recoveryform' : form})
	

	
def recovered(request, value):				
	if request.session.get('value') == request.path[-keylength:]:
		if request.method == 'POST':
			form = RecoveryForm2(request.POST)
			if form.is_valid():
				password1 = form.cleaned_data.get('password1')
				password2 = form.cleaned_data.get('password2')
				if password1 == password2:
					user = User.objects.get(username=request.session.get('user'))
					user.password = password1
					user.save()
					request.session.flush()
					return render(request, 'main/auth/recovered.html', {'form': form, 'change' : True})
				else:
					return render(request, 'main/auth/recovered.html', {'form': form, 'error' : True})
		else:
			form = RecoveryForm2()
			return render(request, 'main/auth/recovered.html', {'form': form })
	else:
		print(request.path[-keylength:])
		print(request.session.get('value'))
		return redirect(reverse('recovery'))
		
		
		
def notfound(request):
	return render(request, 'main/error/404.html')
	
def oopsiewoopsie(request):
	return render(request, 'main/error/500.html')